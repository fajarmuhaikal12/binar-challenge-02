const changeWord =(selectedText,changedText,text) => {
    let result = text.replace(selectedText,changedText)
    return result
}

const kalimat1 = 'Budi pergi liburan ke Bali'
const kalimat2 = 'Gunung bromo tak mampu menggambarkan besarnya cintaku padamu'

console.log(changeWord('padamu','padanya',kalimat2))
console.log(changeWord('Budi','Ojan',kalimat1))
