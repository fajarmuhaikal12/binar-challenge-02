const checkEmail = (email) => {
    let regexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
    if (regexEmail.test(email)){return ('VALID')}
    if(email == null){return ('Please input your email first')}
    if(typeof email != 'string'){return ('Your email must string data type')}
    if(!email.includes('@')){return ('email must have @ character')}
    
    return ('INVALID')
}

console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata')) 
console.log(checkEmail(3322))
console.log(checkEmail())