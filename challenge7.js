const dataPenjualanPakAdi = [
    {
        namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan : 760000,
        kategori : 'Sepatu Sport',
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warrior Tristan Black',
        hargaSatuan : 960000,
        kategori : 'Sepatu Sneaker',
        totalTerjual : 37,
    },
    {
        namaProduct : 'Sepatu Warior Tristan Maroon',
        hargaSatuan : 360000,
        kategori : 'Sepatu Sneaker',
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warrior Rainbow Tosca',
        hargaSatuan : 120000,
        kategori : 'Sepatu Sneaker',
        totalTerjual : 90,
    },
]

const hitungTotalPenjualan = (dataPenjualan) => {
    let total = dataPenjualan.filter(dataPenjualan => dataPenjualan.totalTerjual).map(dataPenjualan => dataPenjualan.totalTerjual)
    .reduce( (total, curr )=> total + curr) ;

    return total
}
 
// const hitungTotalPenjualan = dataPenjualanPakAdi.filter(dataPenjualan => dataPenjualan.totalTerjual).map(dataPenjualan => dataPenjualan.totalTerjual)
// .reduce( (total, curr )=> total + curr) ;

console.log(hitungTotalPenjualan(dataPenjualanPakAdi))