const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
];

const getInfoPenjualan =(dataPenjualan)=>{
    let hasilAkhir ={}
    let totalNovelTerjual =  dataPenjualan.filter(dataPenjualan => dataPenjualan.totalTerjual).map(dataPenjualan => dataPenjualan.totalTerjual).reduce( (total, curr )=> total + curr) ;

    let totalHargaBeli = dataPenjualan.filter(dataPenjualan => dataPenjualan.hargaBeli).map(dataPenjualan => dataPenjualan.hargaBeli).reduce( (total, curr )=> total + curr) ;

    let totalHargaJual = dataPenjualan.filter(dataPenjualan => dataPenjualan.hargaJual).map(dataPenjualan => dataPenjualan.hargaJual).reduce( (total, curr )=> total + curr) ;

    let totalSisaStok = dataPenjualan.filter(dataPenjualan => dataPenjualan.sisaStok).map(dataPenjualan => dataPenjualan.sisaStok).reduce( (total, curr )=> total + curr) ;

    let totalModal = (totalNovelTerjual+totalSisaStok)*totalHargaBeli

    let totalKeuntungan = (totalHargaJual - totalHargaBeli ) * totalNovelTerjual

    let persentaseKeuntungan = (totalKeuntungan/((totalNovelTerjual+totalSisaStok)*totalHargaJual)) *100

    let dataBuku = dataPenjualan.reduce((prev, curr) => {
        const index = prev.findIndex(search => search.namaProduk === curr.namaProduk)
        if(index !== -1){
          const data = [...prev]
          data[index].totalTerjual += curr.totalTerjual
          return [...data]
        }
    
        return [...prev, curr]
      }, []);
    let produkBukuTerlaris = dataBuku.reduce((prev, curr ) => { return curr.totalTerjual > prev.totalTerjual ? curr:prev.namaProduk }) ;
    

    let dataPenulis = dataPenjualan.reduce((prev, curr) => {
        const index = prev.findIndex(search => search.penulis === curr.penulis)
        if(index !== -1){
          const data = [...prev]
          data[index].totalTerjual += curr.totalTerjual
          return [...data]
        }
    
        return [...prev, curr]
      }, []);
    
      let penulisTerlaris = dataPenulis.reduce((prev, curr) => {
        return prev.totalTerjual > curr.totalTerjual ? prev : curr.penulis;
    });
    

    hasilAkhir.totalKeuntungan = `Rp.${new Intl.NumberFormat('id-ID').format(Math.floor(totalKeuntungan))}`;
    hasilAkhir.totalModal = `Rp.${new Intl.NumberFormat('id-ID').format(Math.floor(totalModal))}`;
    hasilAkhir.persentaseKeuntungan = persentaseKeuntungan.toFixed(2)  + '%';
    hasilAkhir.penulisTerlaris = penulisTerlaris.penulis;
    hasilAkhir.bukuTerlaris = produkBukuTerlaris;

    return hasilAkhir

}
    

console.log(getInfoPenjualan(dataPenjualanNovel))