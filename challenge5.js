const getSplitName = (personName) => {

    const nama = personName.toString().split(' ')
    const lengthNama = nama.length

    if(nama.toString().match(/^[0-9]+$/)){return  `Error : This function is only for string name`;}

    switch(lengthNama){
        case 1 :
            return {
                firstName : nama[0],
                middleName : null,
                lastName : null
            }
        case 2 :
            return {
                firstName : nama[0],
                middleName : nama[1],
                lastName : null  
            }
        case 3 :
            return {
                firstName : nama[0],
                middleName : nama[1],
                lastName : nama[2]
            }
        default:
            return 'Error : this function is only for 3 characters name '
    }   
}

console.log(getSplitName('Fajar Muhaikal Amal Amal'))
console.log(getSplitName('Fajar Muhaikal Amal'))
console.log(getSplitName('Fajar Muhaikal'))
console.log(getSplitName('Fajar'))
console.log(getSplitName(0))

