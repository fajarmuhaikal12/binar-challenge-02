const isValidPassword = (givenPassword) => {
    let regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/

    if(regexPassword.test(givenPassword)){return ('true')}
    if(givenPassword == null){return ('Error : please enter your password')}
    if(typeof givenPassword !== 'string' ){return('Error : please enter your password in the string data type')}
    return ('false')
}

console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())
